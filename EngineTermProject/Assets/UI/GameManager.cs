﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameState state;

    private void Start()
    {
        EventManager.On("gameStarted", OnGameStarted);
        EventManager.On("gamePaused", OnGamePaused);
        EventManager.On("gameExit", OnGameExit);
    }


    private void OnGameStarted(object obj) => state = GameState.Playing;
    private void OnGamePaused(object obj) => state = GameState.Paused;
    private void OnGameExit(object obj) => state = GameState.Ended;
}
