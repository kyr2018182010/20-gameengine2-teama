﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : SingletonBehaviour<EventManager>
{
    private Dictionary<string, List<Action<object>>> _eventDatabase;

    private void Awake()
    {
        // 새 딕셔너리 생성
        _eventDatabase = new Dictionary<string, List<Action<object>>>();
    }

    public static void On(string eventName, Action<object> subscriberAction)
    {
        if(instance._eventDatabase.ContainsKey(eventName)== false)      // 이벤트 데이터베이스에 이벤트가 없으면
        {
            // 이벤트 데이터베이스에 이벤트를 추가해준다.
            instance._eventDatabase.Add(eventName, new List<Action<object>>());
        }

        // 있으면 액션 추가
        instance._eventDatabase[eventName].Add(subscriberAction);
    }

    public static void Emit(string eventName, object parameter)
    {
        if(instance._eventDatabase.ContainsKey(eventName) == false)
        {
            return;
        }
        
        foreach(var action in instance._eventDatabase[eventName])
        {
            action.Invoke(parameter);
        }
    }
}
