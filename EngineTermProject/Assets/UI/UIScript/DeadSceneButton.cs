﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadSceneButton : MonoBehaviour
{
    public void clickRestart()
    {
        // 시작 메뉴로 다시 돌아감
        // _sceneName = "";
        // SceneManager.LoadScene(_sceneName);
    }

    public void clickExit()
    {
        Application.Quit(); // 어플리케이션 종료
    }
}
