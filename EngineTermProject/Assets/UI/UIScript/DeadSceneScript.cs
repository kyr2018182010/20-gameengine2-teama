﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadSceneScript : MonoBehaviour
{
    private float twinkleTime;

    private void Update()
    {
        if(twinkleTime < 0.5f)
        {
            GetComponent<SpriteRenderer>().color = new Color(255f, 254f, 198f, 1.0f - twinkleTime);
        }
        else
        {
            GetComponent<SpriteRenderer>().color = new Color(255f, 254f, 198f, twinkleTime);
            if(twinkleTime > 1f)
            {
                twinkleTime = 0;
            }
        }
        twinkleTime += Time.deltaTime;
    }
}
