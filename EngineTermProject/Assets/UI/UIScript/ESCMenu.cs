﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.SceneManagement;

public class ESCMenu : MonoBehaviour
{
    [SerializeField] private GameObject escMenu = null;

    private GameState _gameState = GameState.Playing;
    private bool isPaused;
    private string _sceneName;

    private void Start()
    {
        _sceneName = null;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;
        }
        if (isPaused)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    public void Hide()
    {
        escMenu.SetActive(false);
        Time.timeScale = 1;
        isPaused = false;
        // _gameState = GameState.Playing;
    }
    private void Show()
    {
        escMenu.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;
        // _gameState = GameState.Paused;
    }

    public void GoToTitle()
    {
        // 타이틀 씬 구현하면 씬매니저로 바꿔주기
        // _sceneName = "";
        // SceneManager.LoadScene(_sceneName);
    }

    public void ExitGame()
    {
        //Debug.Log("exit");
        //Application.Quit();
        Application.Quit(); // 어플리케이션 종료

    }

}
