﻿using System;

using System.Collections.Generic;

[Serializable]
public struct QuestData
{
    public List<NPCData> npcInQuest;
}
