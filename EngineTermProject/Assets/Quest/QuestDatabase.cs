﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="quest_database", menuName ="Quest 데이터베이스 만들기")]
public class QuestDatabase:ScriptableObject
{
    [SerializeField] public List<QuestData> QuestDataList;
}
