﻿using Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjInteraction : MonoBehaviour
{
    [SerializeField] private GameObject otherObj;
    [SerializeField] private QuestManager questManager;

    // 트리거 안으로 들어와야 할 때 = true, 나가야 할 때 = false
    [SerializeField] private bool inOrOut = false;

    private void OnTriggerEnter(Collider other)
    {
        if (inOrOut)
        {
            if(other.gameObject == otherObj)
            {
                questManager.nextActionOk = true;
                questManager.questZone = this.gameObject;
            }
        }
        else
        {
            if (other.gameObject == otherObj)
            {
                questManager.nextActionOk = false ;
                questManager.questZone = null;

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!inOrOut)
        {
            if (other.gameObject == otherObj)
            {
                questManager.nextActionOk = false;
                questManager.questZone = null;
            }
        }
        else
        {
            if (other.gameObject == otherObj)
            {
                questManager.nextActionOk = true;
                questManager.questZone = this.gameObject;
            }
        }
    }
}
