﻿using System;
using System.Collections.Generic;
using System.Xml;
using Player.Scripts;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Script
{
    public class QuestManager : MonoBehaviour
    {
        List<QuestData> questDatas;
        int currentQuestIdx=0;
        int currentNpcIdx=0;
        int dialogIdx = 0;

        public bool isAction = false;
        public bool nextActionOk = false;
        // quest 완료 후 QuestZone 없애주기
        public GameObject questZone = null;
        private GameObject selectedObj;
        
        [SerializeField] private PlayerController player;

        // 대화 패널
        [SerializeField] private Image panel;
        [SerializeField] private TextMeshProUGUI text;

        // 퀘스트 패널
        [SerializeField] private Image panel2;
        [SerializeField] private TextMeshProUGUI text2;

        // 선택지 버튼
        [SerializeField] private Button Button1;
        [SerializeField] private Button Button2;

        // 선택지 텍스트
        [SerializeField] private TextMeshProUGUI Button1Text;
        [SerializeField] private TextMeshProUGUI Button2Text;

        void Start()
        {
            questDatas = new List<QuestData>();
            LoadXml();
        }

        public int GetQuestIdx(int npcId)
        {
            return currentQuestIdx;
        }

        public void ScanObject(GameObject obj)
        {
            if (dialogIdx == questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].dialogs.Count)
            {
                dialogIdx = 0;
                // 다음 다이얼로그로
                if(nextActionOk || questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].nextActionOk)
                {
                    Button1.gameObject.SetActive(false);
                    Button2.gameObject.SetActive(false);
                    panel2.gameObject.SetActive(false);

                    if (questZone != null)
                        questZone.SetActive(false);

                    nextActionOk = false;
                    ++currentNpcIdx;
                    
                }
                // 다음 퀘스트로
                if(currentNpcIdx == questDatas[currentQuestIdx].npcInQuest.Count)
                {
                    currentNpcIdx = 0;
                    ++currentQuestIdx;
                }

                isAction = false;
            }
            else{
                Debug.Log("npc check");
                NPC npc = obj.GetComponent<NPC>();
                if (questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].npcId == npc.id)
                {
                    selectedObj = obj;
                    Debug.Log("npc right");

                    // NPC 대화 카메라의 LookAt 조정을 위한 transform 값을 PlayerController에 넘겨준다.
                    var transform = obj.GetComponentsInChildren<Transform>();
                    player.npcTransform = transform[1];
                    // 플레이어의 상태를 대화상태로 바꿔준다.
                    player.ConversationStart();
                    
                    isAction = true;
                    text.text = questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].dialogs[dialogIdx];
                    dialogIdx++;
                }
            }

            // 주인공 선택지 버튼
            if (questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].npcId == 0)
            {
                Button1.gameObject.SetActive(true);
                Button2.gameObject.SetActive(true);

                Button1Text.text = questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].dialogs[dialogIdx++];
                Button2Text.text = questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].dialogs[dialogIdx++];
            }

            // 다음 퀘스트를 하러 가자 !
            if (questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].npcId == 1)
            {
                selectedObj = null;
                player.ConversationEnd();
                panel2.gameObject.SetActive(true);
                text2.text = questDatas[currentQuestIdx].npcInQuest[currentNpcIdx].dialogs[dialogIdx++];
                Invoke("HidePanel2", 3f);
            }

            panel.gameObject.SetActive(isAction);
        }

        void HidePanel2()
        {
            panel2.gameObject.SetActive(false);
        }
        void LoadXml()
        {
            XmlDocument xmlDoc = new XmlDocument();

            // XML 데이타를 파일에서 로드
            xmlDoc.Load("./Assets/Quest/Quest.xml");

            XmlNodeList nodes = xmlDoc.SelectNodes("QuestInfo/Quest");

            foreach (XmlNode node in nodes)
            {
                QuestData questData;
                List<NPCData> npcs = new List<NPCData>();

                XmlNodeList npcNodes = node.SelectNodes("NPC");
                foreach (XmlNode npcNode in npcNodes)
                {
                    int npcid = int.Parse(npcNode.SelectSingleNode("npcID").InnerText);
                    bool nextActionOk = bool.Parse(npcNode.SelectSingleNode("nextActionOk").InnerText);

                    List<string> dialog = new List<string>();

                    XmlNodeList dialogNodes = npcNode.SelectNodes("DIALOG/dialogs");
                    foreach (XmlNode dialogNode in dialogNodes)
                    {
                        dialog.Add(dialogNode.InnerText);
                    }

                    npcs.Add(new NPCData(npcid, dialog, nextActionOk));
                }
                questData.npcInQuest = npcs;
                questDatas.Add(questData);
            }

        }
        
                
        public void ClickedNext()
        {
            ScanObject(selectedObj);
        }


        public void ClickedButton()
        {
            nextActionOk = true;
            ScanObject(selectedObj);
            ScanObject(selectedObj);
        }

    }
}