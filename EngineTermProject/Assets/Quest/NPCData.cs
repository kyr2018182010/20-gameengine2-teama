﻿using System;
using System.Collections.Generic;


[Serializable]
public struct NPCData
{
    public int npcId;
    public List<string> dialogs;
    public bool nextActionOk;
    public NPCData(int id, List<string> logs, bool ok)
    {
        npcId = id;
        dialogs = logs;
        nextActionOk = ok;
    }
}
