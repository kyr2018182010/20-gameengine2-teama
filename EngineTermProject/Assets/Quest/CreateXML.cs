﻿using System.Xml;
using UnityEngine;


public class CreateXML : MonoBehaviour
{
    [SerializeField]
    private QuestDatabase questDatabase;

    void Start()
    {
        CreateXml();
    }

    void CreateXml()
    {
        XmlDocument xmlDoc = new XmlDocument();

        // Xml을 선언한다(xml의 버전과 인코딩 방식을 정해준다.)
        xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "yes"));

        // 루트 노드 생성
        XmlNode root = xmlDoc.CreateNode(XmlNodeType.Element, "QuestInfo", string.Empty);
        xmlDoc.AppendChild(root);


        // dialogData 안에 있는 모든 다이얼로그 생성
        foreach (QuestData quest in questDatabase.QuestDataList)
        {
            // 자식 노드 생성
            XmlNode child = xmlDoc.CreateNode(XmlNodeType.Element, "Quest", string.Empty);

            // 자식 노드에 들어갈 속성 생성
            for (int i = 0; i < quest.npcInQuest.Count; ++i)
            {
                XmlNode child1 = xmlDoc.CreateNode(XmlNodeType.Element, "NPC", string.Empty);

                XmlElement npcID = xmlDoc.CreateElement("npcID");
                npcID.InnerText = quest.npcInQuest[i].npcId.ToString();
                child1.AppendChild(npcID);

                XmlElement nextActionOk = xmlDoc.CreateElement("nextActionOk");
                nextActionOk.InnerText = quest.npcInQuest[i].nextActionOk.ToString();
                child1.AppendChild(nextActionOk);

                XmlNode child2 = xmlDoc.CreateNode(XmlNodeType.Element, "DIALOG", string.Empty);

                for (int n = 0; n < quest.npcInQuest[i].dialogs.Count; ++n)
                {
                    XmlElement dialogs = xmlDoc.CreateElement("dialogs");
                    dialogs.InnerText = quest.npcInQuest[i].dialogs[n];
                    child2.AppendChild(dialogs);
                }
                child1.AppendChild(child2);
                child.AppendChild(child1);
            }
            root.AppendChild(child);

        }
        xmlDoc.Save("./Assets/Quest/Quest.xml");
    }
}
