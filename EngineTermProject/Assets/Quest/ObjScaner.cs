﻿using UnityEngine;

namespace Script
{
    public class ObjScaner : MonoBehaviour
    {
        [SerializeField] QuestManager questManager;
        private GameObject ScanedObj;
        private void FixedUpdate()
        {
            if (questManager.isAction == false)
            {
                RaycastHit hit;

                Vector3 rayDirection = transform.TransformDirection(Vector3.forward);
                Vector3 rayPosition = new Vector3(transform.position.x, transform.position.y + 1.2f, transform.position.z);
               
                Physics.Raycast(rayPosition, rayDirection, out hit, 7, LayerMask.GetMask("Object"));
                Debug.DrawRay(rayPosition, rayDirection * 7, Color.red);
                if (hit.collider != null)
                {
                    ScanedObj = hit.transform.gameObject;
                    questManager.ScanObject(ScanedObj);
                }
            }
           
        }
        public void ClickedNext()
        {
            questManager.ScanObject(ScanedObj);
        }


        public void ClickedButton()
        {
            questManager.nextActionOk = true;
            questManager.ScanObject(ScanedObj);
            questManager.ScanObject(ScanedObj);
        }
    }
}