﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GravityObjState 
{
    MoreLight,
    Light,
    Middle,
    Heavy,
    MoreHeavy
}
