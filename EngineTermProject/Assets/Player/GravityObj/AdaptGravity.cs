﻿using UnityEngine;


public class AdaptGravity : MonoBehaviour
{  [SerializeField] private GravityObjState _state;
   //[SerializeField] private GravityObjState defaultState;
   [SerializeField] private GravityStateInfo info;

   private Rigidbody _rigidbody;

   private float _addedGravity;
   private float _tempAddedGravity;
   [SerializeField] private float gravity = 9.8f;

   [SerializeField] private float power;
   [SerializeField] private bool stick; 

    private Color _defaultColor;
    private float _prevAxis;
    

    private float _sec;
    public float maxSec = 15f;

    private float _currentVelocity;

    private float _defaultY;
    private float _minY;
    private float _maxY;

    private bool _selecting;

    // UI
    private Renderer _renderer;
    private Material _defaultMat;

    void Awake()
    {
        SetState();
        if(stick)
            _currentVelocity = 0f;
        else
            _currentVelocity = info.velocities[(int) _state];
        _rigidbody = GetComponent<Rigidbody>();
        _defaultY = transform.position.y;
        Transform[] transforms = GetComponentsInChildren<Transform>();
        if (transforms[1] != null)
        {
            _minY = transforms[1].position.y;
            //Destroy(transforms[1].gameObject);
        }

        if(transforms[2] != null)
        {
            _maxY = transforms[2].position.y;
            //Destroy(transforms[2].gameObject);
        }
        _renderer = GetComponent<Renderer>();
        if (_renderer == null)
        {
            Debug.LogError("Renderer Not Loaded!");
            return;
        }
        _defaultMat = _renderer.material;
    }

    void FixedUpdate()
    {
        if (_selecting)
        {
            AddGravity();
        }

        if (stick)
        {
            _currentVelocity += -(_addedGravity + gravity) * Time.deltaTime;
            if (_addedGravity + gravity > 0f)
            {
                _currentVelocity = info.velocities[(int) _state];
            }
            else if (_addedGravity + gravity < 0f)
            {
                _currentVelocity = info.velocities[(int) _state];
            }
        }

        transform.position += Vector3.up * (_currentVelocity * Time.deltaTime);
        if (_minY >= transform.position.y && _currentVelocity <= 0f)
        {
            transform.position = new Vector3(transform.position.x, _minY, transform.position.z);
            _currentVelocity = 0;
        }
        else if (_maxY <= transform.position.y && _currentVelocity >= 0)
        {
            transform.position = new Vector3(transform.position.x, _maxY, transform.position.z);
            _currentVelocity = 0;
        }

        if (_addedGravity != 0f)
        {
            _sec += Time.deltaTime;
            if (_sec >= maxSec)
            {
                _sec = 0f;
                _addedGravity = 0f;
                SetState();
                if(stick)
                    _currentVelocity = 0f;
                else
                    _currentVelocity = info.velocities[(int) _state];
            }
        }

        if (_state <= GravityObjState.Middle)
            _rigidbody.mass = 1f;
        else
        {
            _rigidbody.mass = 100f;
        }
    }

    private void AddGravity()
    {
        // Input
        float axis = Input.GetAxis("Mouse ScrollWheel");

        if (_prevAxis * axis < 0)
        {
            _tempAddedGravity = 0f;
            _currentVelocity = 0f;
        }

        if (axis < 0)
        {
            _tempAddedGravity += power;
            if (_tempAddedGravity + gravity > info.maxGravity)
            {
                _tempAddedGravity = info.maxGravity - gravity;
            }
        }
        else if (axis > 0)
        {
            _tempAddedGravity -= power;
            if (_tempAddedGravity + gravity < info.minGravity)
            {
                _tempAddedGravity = info.maxGravity + gravity;
            }
        }

        // set State
        SetState();
        
        _prevAxis = axis;

        // UI
        var color = _renderer.material.GetColor("_EmissionColor");
        color.r = info.colors[(int) _state].r;
        color.g = info.colors[(int) _state].g;
        color.b = info.colors[(int) _state].b;
        _renderer.material.SetColor("_EmissionColor",color);
   }

    void SetState()
    {
        var curGravity = _tempAddedGravity + gravity;
        if (curGravity < info.bases[0])
            _state = GravityObjState.MoreLight;
        else if (curGravity >= info.bases[0] && curGravity < info.bases[1])
            _state = GravityObjState.Light;
        else if (curGravity >= info.bases[1] && curGravity < info.bases[2])
            _state = GravityObjState.Middle;
        else if (curGravity >= info.bases[2] && curGravity < info.bases[3])
            _state = GravityObjState.Heavy;
        else if(curGravity >= info.bases[3])
            _state = GravityObjState.MoreHeavy;
    }

    public void SelectObject(Material mat)
    {
        // 선택시 메터리얼을 중력 UI용 메터리얼로 변경
        _renderer.material = mat;
        _selecting = true;
        _defaultColor = mat.GetColor("_EmissionColor");
    }

    public void AdaptChanges()
    {
        // set velocity
        if(!stick)
            _currentVelocity = info.velocities[(int) _state];
        _addedGravity = _tempAddedGravity;
        _tempAddedGravity = 0f;
        _renderer.material = _defaultMat;
        _selecting = false;
        if (stick)
            _minY -= 50f;
    }
}



