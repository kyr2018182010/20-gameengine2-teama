﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GravityStateInfo : MonoBehaviour
{
    public List<Color> colors;
    public List<float> bases;
    public List<float> velocities;

    public float maxGravity;
    public float minGravity;
}
