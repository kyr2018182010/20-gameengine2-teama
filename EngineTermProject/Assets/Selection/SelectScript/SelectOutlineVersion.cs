﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectOutlineVersion : MonoBehaviour
{
    [SerializeField] private string selectableTag = "selectable";

    private ISelectionResponse _selectionResponse;

    RaycastHit hit;
    private Transform _selection;

    private void Awake()
    {
        _selectionResponse = GetComponent<ISelectionResponse>();
    }

    private void Update()
    {
        // 선택 안했을 때
        if (_selection != null)
        {
            _selectionResponse.OnDeselect(_selection);
        }


        // 레이캐스트 만들기
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        _selection = null;
        // 선택하는 거
        if (Physics.Raycast(ray, out hit))
        {
            var selection = hit.transform;

            if (selection.CompareTag(selectableTag))
            {
                _selection = selection;
            }
        }

        if(_selection != null)
        {
            _selectionResponse.OnSelect(_selection);

        }
    }
}

