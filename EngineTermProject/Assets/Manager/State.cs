using System.Collections.Generic;

public enum State
    {
        Initializing,
        SceneLoading,
        SceneLoaded,
        Playing,
        Paused,
        GameEnded
    }
